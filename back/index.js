const express = require('express');
const cors = require('cors');
const {nanoid} = require('nanoid');
const app = express();
const exitHook = require('async-exit-hook');
const config = require('./config');
////////////////////////////////
const users = require('./app/users');
const mongoose = require("mongoose");
const User = require("./models/User");
const Message = require("./models/Message");

require('express-ws')(app);

const port = 8000;

app.use(express.json());
app.use(cors());

app.use('/users', users);

const activeConnections = {};
let activeUsers = [];

app.ws('/messages', async (ws, req) => {
    const id = nanoid();
    console.log('Client connected!, id = ', id);
    activeConnections[id] = ws;
    ///////////////////////////
    const user = await User.findOne({token: req.param('token')});
    activeUsers = [...activeUsers, {username: user.username, id: user._id}];

    const messages = await Message.find().sort({_id: 1}).limit(30);
    ws.send(JSON.stringify({type: 'CONNECTED', activeUsers, messages: messages}));

    Object.keys(activeConnections).forEach(key => {
        const connection = activeConnections[key];

        connection.send(JSON.stringify({
            type: 'NEW_USER',
            user: {username: user.username, id: user._id},
        }));

    });


    ws.on('message', msg => {
        const decoded = JSON.parse(msg);

        if(decoded.type === 'CREATE_USER') {
            Object.keys(activeConnections).forEach(key => {
                const connection = activeConnections[key];
                connection.send(JSON.stringify({
                    type: 'NEW_USER',
                    user: decoded.user,
                }));
            });
        }

        if(decoded.type === 'CREATE_MESSAGE') {
            // const message = Message.save(decoded.message);

            Object.keys(activeConnections).forEach(key => {
                const connection = activeConnections[key];
                connection.send(JSON.stringify({
                    type: 'NEW_MESSAGE',
                    message: decoded.message,
                }));
            });
        }
    })

    ws.on('close', () => {
        console.log('Client disconnected!, id = ', id);
        delete activeConnections[id];
        activeUsers = activeUsers.filter(activeUser => activeUser.id !== user._id);
    });
});

const run = async () => {
    await mongoose.connect(config.db.url, config.db.options);

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });

    exitHook(async callback => {
        await mongoose.disconnect();
        console.log('mongoose disconnected');
        callback();
    });
}

run().catch(console.error);