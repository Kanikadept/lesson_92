import {Route, Switch} from 'react-router-dom';
import './App.css';
import Layout from "./components/Layout/Layout";
import Register from "./containers/Register/Register";
import LogIn from "./containers/Login/LogIn";
import Main from "./containers/Main/Main";

const App = () => (
    <div className="App">
        <div className="container">
            <Layout>
                <Switch>
                    <Route path="/" component={LogIn} exact/>
                    <Route path="/register" component={Register} exact/>
                    <Route path="/chat" component={Main} exact/>
                </Switch>
            </Layout>
        </div>
    </div>
);

export default App;
