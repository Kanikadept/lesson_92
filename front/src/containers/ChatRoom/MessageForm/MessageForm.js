import React, {useState} from 'react';
import './MessageForm.css';
import {useDispatch} from "react-redux";

const MessageForm = ({ws}) => {

    const dispatch = useDispatch();

    const [message, setMessage] = useState('');

    const handleSubmit = event => {
        event.preventDefault();
        ws.current.send(JSON.stringify({type: 'CREATE_MESSAGE', message}));
    }

    return (
        <form className="message-form" onSubmit={handleSubmit}>
            <div className="message-form__row">
                <input type="text" name="text"
                       value={message}
                       placeholder="Enter message"
                       onChange={(event) => setMessage(event.target.value)}/>
                <button>Send</button>
            </div>

        </form>
    );
};

export default MessageForm;