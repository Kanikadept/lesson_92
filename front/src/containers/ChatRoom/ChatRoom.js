import React from 'react';
import './ChatRoom.css';
import MessageForm from "./MessageForm/MessageForm";
import Messages from "./Messages/Messages";

const ChatRoom = ({ws, messages}) => {
    return (
        <div className="chat-room">
            Chat room
            <Messages messages={messages}/>
            <MessageForm ws={ws}/>
        </div>
    );
};

export default ChatRoom;