import React from 'react';
import './Messages.css';

const Messages = ({messages}) => {
    return (
        <div className="messages">
            {messages.map(message => {
                return <p>{message}</p>
            })}
        </div>
    );
};

export default Messages;