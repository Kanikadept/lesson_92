import React from 'react';
import './OnlineUsers.css';

const OnlineUsers = ({users}) => {
    return (
        <ul className="online-users">
            Online user list
            {users.map((user, i) => {
               return <li key={user.id}>{user.username}</li>
            })}
        </ul>
    );
};

export default OnlineUsers;