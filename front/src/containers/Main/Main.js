import React, {useEffect, useRef, useState} from 'react';
import './Main.css';
import OnlineUsers from "../OnlineUsers/OnlineUsers";
import ChatRoom from "../ChatRoom/ChatRoom";
import {useSelector} from "react-redux";

const Main = () => {

    const user = useSelector(state => state.users.user);
    const [users, setUsers] = useState([]);
    const [messages, setMessages] = useState([]);
    const ws = useRef(null);

    useEffect(() => {
        if (user) {
            ws.current = new WebSocket('ws://localhost:8000/messages?token=' + user.token);

            ws.current.onmessage = event => {
                const decoded = JSON.parse(event.data);

                if (decoded.type === 'CONNECTED') {
                    setUsers([...decoded.activeUsers]);
                    // setMessages([...decoded.messages]);
                }
                if (decoded.type === 'NEW_USER') {
                    setUsers(prev => [...prev, decoded.user]);
                }
                if (decoded.type === 'NEW_MESSAGE') {
                    setMessages(prev => [...prev, decoded.message]);
                }

            };
        }
    }, [user]);

    return (
        <main className="main">
            <OnlineUsers users={users}/>
            <ChatRoom ws={ws} messages={messages}/>
        </main>
    );
};

export default Main;