import React from 'react';
import {NavLink} from "react-router-dom";

import './UserMenu.css';
import {useDispatch} from "react-redux";
import {logoutUser} from "../../../store/actions/usersActions";

const UserMenu = ({user}) => {

    const dispatch = useDispatch();

    const handleLogout = () => {
        dispatch(logoutUser());
    }

    return (
        <div className="user-menu">
            <span>Welcome {user.username}!</span>
            <button onClick={handleLogout}>Logout</button>
        </div>
    );
};

export default UserMenu;